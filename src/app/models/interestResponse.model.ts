export interface InterestResponse {

    INTEREST : number;
    DAYS : number;
    TOTAL_INTEREST:number;
}