export interface UserDetails {

    userName : string;
    password : string;
    authToken : string;
    roles: string;
    id: number;
    sellerId: number;
    authorities: string[];
    refreshToken: string;
	

}